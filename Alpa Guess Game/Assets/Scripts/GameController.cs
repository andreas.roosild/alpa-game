﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
[System.Serializable]
public class GameController : MonoBehaviour
{
    //Content
    [SerializeField]
    public Question[] questions;
    public Question[] randomQuestions;

    //Canvas
    public GameObject item;
    public Button[] buttons;
    public GameObject alpa;

    //Parameters
    int currentQuestionIndex = 0;
    bool timerStarted = false;
    int timer = 0;

    //Sounds
    AudioSource audioSource;
    public AudioClip tubli;
    public AudioClip vali;
    public AudioClip kasMeeldib;
    public AudioClip wrong;
    public AudioClip correct;

    public AudioSource GetAudioSource()
    {
        return audioSource;
    }


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        randomQuestions = new Question[questions.Length];
        ShuffleQuestions();
        SetQuestionReady();
    }

    private void Update()
    {
        //This is to count 2 seconds
        if (timerStarted)
        {
            timer++;
        }
    }

    public void SetQuestionReady()
    {
        if (currentQuestionIndex >= questions.Length)
        {
            GameOver();
            return;
        }
        audioSource.PlayOneShot(vali);
        Question currentQuestion = randomQuestions[currentQuestionIndex];
        //Sets the question illustration
        item.GetComponent<Image>().sprite = currentQuestion.itemGraphics;
        //Set up word order randomise
        List<int> numbers = new List<int>{ 0, 1, 2 }.OrderBy(i => UnityEngine.Random.value).ToList();
        //Set words list
        string[] words = new string[3];
        words[numbers[0]] = currentQuestion.itemWord;
        //TODO: pick random similar words, so you can add more to similar words list
        words[numbers[1]] = currentQuestion.similarWords[0];
        words[numbers[2]] = currentQuestion.similarWords[1];

        for (int i = 0; i < words.Length; i++)
        {
            buttons[i].GetComponentInChildren<Text>().text = words[i];
            buttons[i].onClick.RemoveAllListeners();
            buttons[i].GetComponent<ButtonController>().ClearTarget();
            if (words[i].Equals(currentQuestion.itemWord))
            {
                buttons[i].onClick.AddListener(Correct);
                buttons[i].GetComponent<ButtonController>().IsCorrect = true;
            }
            else
            {
                buttons[i].GetComponent<ButtonController>().IsCorrect = false;
            }
        }

        //Next question index
        currentQuestionIndex++;
    }

    private void GameOver()
    {
        item.SetActive(false);

        foreach (Button b in buttons)
        {
            b.gameObject.SetActive(false);
        }
        //Throw in the alpaka
        alpa.SetActive(true);
        alpa.GetComponent<Animator>().Play("AlpaTime");
        PlayEndSounds();
    }

    public void PlayEndSounds()
    {
        audioSource.clip = kasMeeldib;
        audioSource.PlayOneShot(tubli);
        audioSource.PlayDelayed(tubli.length + 2);
    }

    public void PlayRoundStart()
    {

    }

    private void Correct()
    {
        foreach (Button but in buttons)
        {
            but.GetComponent<Button>().interactable = false;
        }
        //Play correct sound and animation
        PlayAnimation();
    }

    public void SetButtonsActive()
    {
        foreach (Button but in buttons)
        {
            but.GetComponent<Button>().interactable = true;
        }
    }

    private void PlayAnimation()
    {
        buttons[0].GetComponent<ButtonController>().FadeOut();
        buttons[1].GetComponent<ButtonController>().FadeOut();
        buttons[2].GetComponent<ButtonController>().FadeOut();
    }

    public void Replay()
    {
        SceneManager.LoadScene("MainScene");
    }

    private void ShuffleQuestions()
    {
        //Shuffles numbers
        List<int> numbers = new List<int>();
        for (int i = 0; i < questions.Length; i++)
        {
            numbers.Add(i);
        }
        numbers = numbers.OrderBy(i => UnityEngine.Random.value).ToList();
        //Sets the random order
        for (int i = 0; i < questions.Length; i++)
        {
            randomQuestions[i] = questions[numbers[i]];
        }
    }
}
