﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trigger : StateMachineBehaviour
{
    bool hasPlayedTranslate = false;
    bool hasPlayedStars = false;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject.Find("Background").GetComponent<Animator>().Play("Darken");
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.normalizedTime > 0.8f && hasPlayedTranslate == false)
        {
            GameObject.Find("Item").GetComponent<Animator>().Play("TranslateOut");
            GameObject.Find("Background").GetComponent<Animator>().Play("Lighten");
            hasPlayedTranslate = true;
        }
        if (stateInfo.normalizedTime > 0.1f && hasPlayedStars == false)
        {
            GameObject.Find("Stars").GetComponent<Animator>().Play("Explode");
            hasPlayedStars = true;
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameObject.Find("GameController").GetComponent<GameController>().SetQuestionReady();
        hasPlayedTranslate = false;
        hasPlayedStars = false;
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
