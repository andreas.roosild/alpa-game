﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class LayoutHelper : MonoBehaviour
{
    public RectTransform background;

    public float verticalPos;
    public float horizontalPos;

    public float width;
    public float height;

    float y;
    float x;

    public bool move = false;

    void Start()
    {
        y = background.rect.height - background.rect.height / verticalPos;
        x = background.rect.width - background.rect.width / horizontalPos;

        transform.localPosition = new Vector2(x, y);
        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(background.rect.width, background.rect.height);
    }

    // Update is called once per frame
    void Update()
    {
        //Keeps track of the screen size, which is the canvas size
        y = background.rect.height - background.rect.height / verticalPos;
        x = background.rect.width - background.rect.width / horizontalPos;

        transform.GetComponent<RectTransform>().sizeDelta = new Vector2(background.rect.width * width, background.rect.height * height);

        if (move)
        {
            Centrify();
        }
        else
        {
            transform.localPosition = new Vector2(x, y);
        }
    }
    
    //Gives the object adjusted center location where it should move
    public void Centrify()
    {
        var vert = 2;
        var hor = 1;
        Vector2 vect = new Vector2(hor, vert);

        transform.localPosition = Vector2.Lerp(transform.localPosition, vect, Time.deltaTime * 4f);
    }

    //Makes the answered button appear on top of other buttons
    public void SetPos()
    {
        move = true;
        transform.SetAsLastSibling();
    }

    public void ResetPos()
    {
        move = false;
    }
}
