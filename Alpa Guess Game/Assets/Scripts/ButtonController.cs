﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonController : MonoBehaviour
{
    //Parameters
    bool isCorrect = false;
    public GameObject imageTarget;


    private void Start()
    {
        ClearTarget();
    }

    public void SetTarget()
    {
        gameObject.GetComponentInParent<LayoutHelper>().SetPos();
    }

    public void ClearTarget()
    {
        gameObject.GetComponentInParent<LayoutHelper>().ResetPos();
    }

    public bool IsCorrect
    {
        get { return isCorrect; }
        set { isCorrect = value; }
    }

    public void PlayAnimation()
    {
        if (isCorrect == false)
        {
            transform.GetComponentInChildren<Animator>().Play("Shiver");
            GameObject.Find("GameController").GetComponent<GameController>().GetAudioSource().PlayOneShot(GameObject.Find("GameController").GetComponent<GameController>().wrong);
        }
    }

    public void FadeOut()
    {
        if (isCorrect == false)
        {
            transform.GetComponentInChildren<Animator>().Play("FadeOut");
        } else
        {
            SetTarget();
            transform.GetComponentInChildren<Animator>().Play("FadeOutLonger"); GameObject.Find("GameController").GetComponent<GameController>().GetAudioSource().PlayOneShot(GameObject.Find("GameController").GetComponent<GameController>().correct);
        }
    }
    
    public void FadeIn()
    {
        transform.GetComponentInChildren<Animator>().Play("FadeIn");
    }
}
