﻿using UnityEngine;
[System.Serializable]
public class Question
{
    public Sprite itemGraphics;
    public string itemWord;
    public string[] similarWords;
}
